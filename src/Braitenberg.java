import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.CommonLCD;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.robotics.LightDetectorAdaptor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class Braitenberg {
	static EV3ColorSensor lightSensorEV3 = new EV3ColorSensor(SensorPort.S4);
	static LightDetectorAdaptor lightAdaptorEV3 = new LightDetectorAdaptor((SampleProvider)lightSensorEV3);
	static NXTLightSensor lightSensor = new NXTLightSensor(SensorPort.S1);
	static LightDetectorAdaptor lightAdaptor = new LightDetectorAdaptor((SampleProvider)lightSensor);
	
	public static void main(String[] args) {
		EV3 ev3 = (EV3) BrickFinder.getDefault();
		GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
		while(true){
			float valueEV3 = lightAdaptorEV3.getLightValue();
			float value = lightAdaptor.getLightValue();
			graphicsLCD.clear();
			graphicsLCD.drawString("EV3 Sensor", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
			graphicsLCD.drawString(""+valueEV3, graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 , GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
			graphicsLCD.drawString("Other Sensor", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2+20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
			graphicsLCD.drawString(""+value, graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 + 40, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
			Delay.msDelay(100);
			
			Thread.yield();
		}
		
	}

}
